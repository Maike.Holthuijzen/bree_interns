\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}


\title{Intern tasks}
\author{Your name here}
\date{\today}

\usepackage[left=1.5cm, right=1.5cm, top=1.5cm]{geometry}
\usepackage[parfill]{parskip}
\usepackage{booktabs}
\usepackage{natbib}
\usepackage{graphicx}
\usepackage{float}
\usepackage{underscore}
\usepackage{amsmath, amssymb, amsthm}
\usepackage{listings}
\usepackage{color}
\usepackage{hyperref}
\usepackage{mathtools}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\allowdisplaybreaks


\begin{document}

\maketitle

\tableofcontents
\newpage

%start sections with \section
\section{Using R Sweave with Knitr compilation}
Make sure to select the knitr option in \begin{verbatim}Tools -> Global Options -> Rsweave -> Knit files using -> knitr \end{verbatim}
<<>>=
myfunction = function(x){
  x + 6
}

myfunction(6)
@


I don't want you to see this code.

<<include=FALSE>>=
another_function = function(x){
  x^2
}
@

Now I'm going to show the result for \texttt{another_function}.
<<>>=
another_function(9)
@


\section{Plots}
Plots are easy to make. You can change height and width as well within the code chunk. Google around to see how you can do this. Always label your figures. Reference figures in your text and provide 2-3 sentences along with your plots that explain what data is being shown in the plot.

%put figures in a figure environment. To ensure figures are placed exactly where they are generated, use the [H] option after 
%\begin{figure}

\begin{figure}[H]
<<>>=
plot(1:10, 1:10)
@

%use \caption{} to add a caption to your figure.
\caption{Add captions to all your figures.}
\end{figure}

\section{Additional information}
Compile your document by hitting ``compile pdf''. If you want bullets or enumerated lists or other formated text, you can click the ``format'' tab and choose options from the dropdown menu. Alternatively, you can find lots of information on the internet on formatting LateX documents.


\end{document}